# Elo 1541L touch driver for Raspberry Pi #

The driver isn't really limited to Raspberry Pi as it isn't architecture dependent and uses only an USB port.

So any Linux distribution on any device, but...

There are three reasons why you want to use it only on Raspberry Pi

* the solution is implemented in Node.js so it is slow
* you probably have to compile a kernel by yourself to use this solution and most people do not like that
* the vendor have finally shipped drivers for armv7 along the i686 and amd64


## Installation ##

If you are still interested

* the driver requires a kernel with DEBUG_FS and UHID flags enabled
* you have to get a cross-compiler, get the kernel sources, change those two flags and cross-compile the kernel
* install nodejs, inotify-tools and xdotool packages

Hint: To test if you need your own kernel check this command

```
#!bash

# cat /sys/kernel/debug/hid/*\:04E7\:0126.*/events
```
Doesn't work? No DEBUG_FS or UHID in kernel.


## Run ##

Driver needs access to /sys/kernel/debug/hid/...


```
#!bash

$ sudo DISPLAY=:0 coffee elo.coffee
```

If it works add it to autostart.


# Notes #

## Vendor drivers for ARM v7, x86 and amd64 ##

There is an official driver for ARM v7 on the vendor site http://www.elotouch.com/Support/Downloads/Driver/DriverDownload/Default.aspx Successfully installed on Arch and Gentoo distributions, on Udroid U3 (Exynos) and Cubieboard 2 (Allwinner) boards.

## It is just a mock/draft ##

If the solution works for you consider rewriting this in C or some other low level language.


# Implementation details #

## Analysis of USB messages ##

```
#!bash

# cat /sys/kernel/debug/hid/*\:04E7\:0126.*/events
```

```
#!log

report (size 14) (numbered) =  0d 2a e6 0b 01 b1 05 04 15 11 d1 07 05 17
report (size 14) (numbered) =  0d 2a ee 0c 01 ae 05 04 14 11 c9 07 05 17
report (size 14) (numbered) =  0d 2a f6 0d 01 ad 05 04 0e 11 c0 07 04 12
report (size 14) (numbered) =  0d 2a fe 0e 01 b2 05 04 09 11 b2 07 04 0a
report (size 10) (numbered) =  09 2b 06 0f 00 11 95 07 02 03
report (size 6) (numbered) =  05 2b 0e 00 00 10
report (size 6) (numbered) =  05 2b 16 01 00 10
report (size 6) (numbered) =  05 2b 1e 02 00 10
report (size 6) (numbered) =  05 2b 26 03 00 10
report (size 14) (numbered) =  0d 2b 4e 04 01 71 07 03 04 11 99 06 04 05
report (size 14) (numbered) =  0d 2b 56 05 01 99 07 04 06 11 8c 06 04 0a
```

05 – **when finger leaves the surface**

0d – time, **coordinates**, two diameters of the ellipsis describing a shape of the finger

09 – seldom present, it's meaning is still a mystery

## Specification ##

**The rest of my notes is in polish (in case you would like to run Google Translate). It is probably easier to just read the code.**

Panel dotykowy Elo 1541L przedstawia się jako się jako

```
04e7:0126  Elo TouchSystems 2515 iTouch Plus USB Touchmonitor

```

Moduł hid-multitouch obsługuje kontroler o podobnej nazwie i przypadkowo przechwytuje nasz panel dzięki czemu jest widoczny na liście urządzeń evdev

```
Elo TouchSystems IntelliTouch Plus
```

Nasz panel nie jest zgodny ze standardem HID, a w jądrze brakuje dedykowanego sterownika (sprawdzone dla jądra 3.11).

## Pominięcie sterownika ##

Wysyłane przez panel komunikaty udało się przechwycić z pominięciem sterownika. Jednoznacznie da się odczytać współrzędne dotyku oraz moment w którym użytkownik odrywa palec. Pozwoliło to na napisanie własnego sterownika.
Sterownik ten obsługuje jeden palec, pozwala klikać w dowolne miejsce ekranu, a także przeciągać elementy. Inne możliwości nie są dostępne.


## Integracja z systemem ##

Sterownik został napisany dla potrzeb projektu sterowania kriokomory w języku CoffeeScript, przy użyciu biblioteki Node.js i znajduje się publicznie pod adresem

Sterownik obserwuje listę podłączonych urządzeń, aktywuje się automatycznie przy podłączaniu i odłączaniu panelu. Do pracy wymaga paczek xrandr, xdotool oraz inotify-tools. Ponieważ interfejs /sys/kenel/debug/hid nie jest dostępny dla zwykłego użytkownika, sterownik jest uruchamiany poleceniem sudo przy starcie środowiska graficznego.

```
#!diff

/etc/sudoers
+ user ALL=(ALL) NOPASSWD: /usr/bin/coffee /home/user/elo/elo.coffee
```

Notatka: Urządzenie znika z folderu /sys/kenel/debug/hid, gdy zostaje odłączone i pojawia się po podłączeniu, często jako inny plik. Sterownik obsługuje ten fakt wykorzystując interfejs systemowy inotify, dzięki któremu reaguje na tworzenie nowych plików w folderze hid.

## Porównanie ze sterownikiem producenta w Windows 8 ##

Sterownik producenta w Windows 8 pozwala obsługiwać dwa palce. Przy trzech ewidentnie nie może się zdecydować i przeskakuje między dwoma trzymanymi bliżej siebie. Zgodnie ze specyfikacją, dwa palce, nie więcej. Dwoma palcami na raz można scrollować. Trzymając jeden i dotykając drugim przywołuje się menu kontekstowe. Na ekranie nie ma widocznego kursora. Sterownik rozwiązuje problem dotarcia do krawędzi poprzez przyspieszenie ruchu zaraz przy nich. Odległość od krawędzi monitora, przy których aktywuje się to rozwiązanie można zmienić, lub wyłączyć. Sterownik pozwala na kalibrację. Monitor posiada firmware w wersji C. Reszta oznaczeń jest zgodna z tymi które widać w Linuksie.

## Przeliczanie współrzędnych ##

Monitor posiada rozdzielczość dotyku 4095x4095 i ekranu 1366x768. Za przeliczenie jednej na drugą odpowiada nasz sterownik. 

## Kalibracja ##

Domyślnie monitor zdaje się być skalibrowany i nie mieć z tym problemów.

## Problem z dwoma palcami ##

Dotknięcie drugim palcem w linuksie powoduje pojawienie się w logach następujących komunikatów


```
#!dmesg

[16403.576371] usb 4-2: uhci_result_common: failed with status 500000
[16403.576376] usb 4-2: input irq status -75 received
[16403.578368] usb 4-2: uhci_result_common: failed with status 500000
[16403.578373] usb 4-2: input irq status -75 received
[16403.580368] usb 4-2: uhci_result_common: failed with status 500000
[16403.580373] usb 4-2: input irq status -75 received
[16403.582368] usb 4-2: uhci_result_common: failed with status 500000
[16403.582373] usb 4-2: input irq status -75 received
```

Komunikaty te nie mają widocznych efektów ubocznych, sam status 500000 oznacza, że urządzenie coś przesyła, mimo, że nie powinno

*The 500000 status means Babble and Stalled.  In other words, the
computer's USB controller detected signals on the USB bus at a time
when the device should not have been sending anything.  Maybe the
signals were electrical noise, or maybe the controller's detection
circuitry isn't working right.*

## Zauważalne problemy ##

Sterownik sprawia problemy w dwóch przypadkach: gdy przyciskamy nie czubkiem palca, tylko większą powierzchnią (lub gdy przesuwamy palec przyciskając), oraz gdy zbyt szybko odrywamy palec od powierzchni. Korzystanie z panelu jest możliwe bez większych trudności, ale pierwsze wrażenie jest często negatywne.

Notatka: Sterownik ze względu na wykorzystane technologie generuje duże obciążenie procesora. Nie jest to w żaden sposób zaskakujące.

## Szybkie przyciśnięcie ##

Szybkie przyciśnięcie i zwolnienie przycisku powoduje, że pierwszy przycisk myszy zostaje wciśnięty, ale nie zostaje zwolniony. W konsekwencji przycisk interfesju graficznego zostaje wciśnięty, ale akcja związana z przyciskiem nie wykonuje się, dokładnie tak jakby użytkownik trzymał przycisk myszy nad przyciskiem i go nie puszczał.

Problem wynika z faktu, że wyświetlacz przy krótkim przyciśnięciu nie wysyła komunikatu oderwania palca. Wygląda to na błąd w firmware, który zostaje ominięty w sterowniku w Windows w jakiś sprytny sposób. Nasz sterownik nie potrafi wydedukować, że ktoś oderwał palec, jeżeli panel nie wysyła żadnej informacji na ten temat.

## Rozmiar palca ##

Dotknięcie zgłaszane przez panel powoduje wciśnięcie klawisza myszy, zwolnienie dotyku zwolnienie klawisza. Istnieje pewna odległość w jakiej muszą się znaleźć współrzędne obu zdarzeń aby system uznał to za kliknięcie. Dotykanie dużą powierzchnią lub przesunięcie palca powoduje zwykle, że pierwsze współrzędne zbyt różni się od drugich aby uznać to za kliknięcie. Odległość odpowiednia dla myszy nie jest odpowiednia dla dotyku palcem. Rozwiązaniem byłoby napisanie sterownika z prawdziwego zdarzenia będącego modułem do jądra, ponieważ sterownik w aktualnej postaci nie ma możliwości zmiany interesującego nas parametru.
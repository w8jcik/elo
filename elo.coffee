fs = require "fs"
readline = require "readline"
exec = require('child_process').exec
inotifywait = require "inotifywait"

# Helpers
  
word_num = (line) ->
  numbered = ""
  position = 0
  for el in line.split(" ")
    numbered += " #{position}:"
    numbered += el
    position += el.length +1
  console.log numbered

# State variables

finger = {}
finger_start = null
dx = dy = 0
last_event = {}
mouse_down = false

puts = (error, stdout, stderr) ->
sh = (cmd, surpress_delay, cb) ->
  cb = puts  unless cb
  
  if surpress_delay == 0
    exec(cmd, puts)
    return
  
  now = new Date()
  last_event[cmd[1..12]] = now  unless last_event[cmd[1..12]]

  delta = now - last_event[cmd[1..12]]
  if delta > surpress_delay or delta < 0
    #console.log "Execute #{cmd}"
    exec(cmd, cb)
    last_event[cmd[1..12]] = new Date()
    
# Configuration

max = 4096

exec 'DISPLAY=:0 xrandr', (error, stdout, stderr) ->
  wh = (stdout.match /(\d+x\d+)/)[0].split("x")
  width = parseInt(wh[0])
  height = parseInt(wh[1])
  
  margin_size = 0.005 * width
  left_margin = right_margin = top_margin = bottom_margin = margin_size
  
  virtual_width = width + left_margin + right_margin
  virtual_height = height + top_margin + bottom_margin
  virtual_width_factor = (virtual_width) / max
  virtual_height_factor = (virtual_height) / max
  
  console.log "Panel size from xrandr", width, "x", height
  console.log "Virtual panel size", Math.round(virtual_width), "x", Math.round(virtual_height)
  
  # Processing

  processing = ->
    hw_dirname = (dir for dir in fs.readdirSync('/sys/kernel/debug/hid/') when dir.match /04E7:0126/)[0]
    return  unless hw_dirname
      
    rd = readline.createInterface(
      input: fs.createReadStream("/sys/kernel/debug/hid/#{hw_dirname}/events")
      output: process.stdout
      terminal: false
    )
    
    rd.on 'error', (err) ->
      console.log "Readline error"
      console.log err
    
    rd.on 'line', (line) ->
      #console.log line
      
      if line.length == 72
        finger.x = ((parseInt("#{line[49...51]}#{line[46...48]}", 16)) * virtual_width_factor) - left_margin
        finger.y = ((max - parseInt("#{line[64...66]}#{line[61...63]}", 16)) * virtual_height_factor) - top_margin
          
        move_cmd = "xdotool mousemove #{(if finger.x < 0 then 0 else finger.x)} #{(if finger.y < 0 then 0 else finger.y)}"
          
        unless mouse_down
          mouse_down = true
          
          sh move_cmd, 16, ->
            sh "xdotool mousedown 1", 0
        else
          sh move_cmd, 16

      if line.length == 47
      
        if mouse_down == true
          mouse_down = false
          sh "xdotool mouseup 1", 0

  processing()

  watch = new inotifywait("/sys/kernel/debug/hid", {recursive: true, watchDirectory: true})
  
  watch.on "add", (filename) ->
    console.log "New device detected."
    if filename.match /04E7:0126/
      console.log "Touch display connected."
      processing()

  watch.on 'error', (err) ->
    console.log "Watch error"
    console.log err
    
process.on 'uncaughtException', (err) ->
  if err.code == 'EIO'
    console.log "Touch display disconnected"
  else
    console.log "Uncaught exception"
    console.log err
